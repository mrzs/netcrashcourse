﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WPFUIItems;

namespace NetCrashCourseDay2
{
    public class CalcSimpleScreen
    {
        private Application app;
        private Window wnd;

        public CalcSimpleScreen(Application app, Window wnd)
        {
            this.app = app;
            this.wnd = wnd;
        }

        public double AddTwoNumbers(double numOne, double numTwo)
        {
            return EnterNumber(numOne)
                .ClickAdd()
                .EnterNumber(numTwo)
                .ClickEquals()
                .GetWhatsOnDisplay();
        }

        public CalcSimpleScreen EnterNumber(double num)
        {
            var numStr = num.ToString("G", CultureInfo.InvariantCulture);
            var parts = numStr.Split('.');
            var integralPart = parts[0];
            EnterDigits(integralPart);
            if (parts.Length > 1)
            {
                var decimalPart = parts[1];
                wnd.Get<Button>("Decimal separator").Click();
                EnterDigits(decimalPart);
            }
            return this;
        }

        private void EnterDigits(String seq)
        {
            foreach (var digit in seq)
            {
                wnd.Get<Button>(digit.ToString()).Click();
            }
        }

        public CalcSimpleScreen ClickAdd()
        {
            wnd.Get<Button>("Add").Click();
            return this;
        }

        public CalcSimpleScreen ClickEquals()
        {
            wnd.Get<Button>("Equals").Click();
            return this;
        }

        public double GetWhatsOnDisplay()
        {
            Label result = wnd.Get<Label>(SearchCriteria.ByAutomationId("150"));
            return double.Parse(result.Text);
        }

        public String GetHistoryDisplay()
        {
            Label history = wnd.Get<Label>(SearchCriteria.ByAutomationId("404"));
            return history.Text;
        }

        public CalcSimpleScreen SwitchToSimple()
        {
            wnd.Get<Menu>("View").Click();
            wnd.Get<Menu>("Standard").Click();
            return new CalcSimpleScreen(app, wnd);
        }

        public CalcScientificScreen SwitchToScientific()
        {
            wnd.Get<Menu>("View").Click();
            wnd.Get<Menu>("Scientific").Click();
            return new CalcScientificScreen(app, wnd);
        }

        public CalcUnitConversionScreen SwitchToUnitConversion()
        {
            wnd.Get<Menu>("View").Click();
            wnd.Get<Menu>("Unit conversion").Click();
            return new CalcUnitConversionScreen(app, wnd);
        }

        public CalcSimpleScreen SwitchToBasicConversion()
        {
            wnd.Get<Menu>("View").Click();
            wnd.Get<Menu>("Basic").Click();
            return this;
        }
    }
}
