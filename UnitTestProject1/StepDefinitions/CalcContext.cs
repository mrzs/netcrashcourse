﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCrashCourseDay2;
using TechTalk.SpecFlow;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;

namespace UnitTestProject1.StepDefinitions
{
    public class CalcContext
    {
        public CalcContext()
        {
            calcApp = Application.Launch("calc.exe");
            calc = calcApp.GetWindow("Calculator");
        }

        public Application calcApp { get; set; }
        public Window calc { get; set; }
    }
}
