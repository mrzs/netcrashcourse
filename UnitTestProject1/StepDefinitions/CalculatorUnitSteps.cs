﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using NetCrashCourseDay2;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.StepDefinitions
{
    [Binding]
    public sealed class CalculatorUnitSteps
    {
        private CalcUnitConversionScreen unitconversionScreen;

        public CalculatorUnitSteps(CalcContext calcContext)
        {
            unitconversionScreen = new CalcUnitConversionScreen(calcContext.calcApp, calcContext.calc);
        }

        [Given(@"I have switched to unit conversion view")]
        public void GivenIHaveSwitchedToUnitConversionView()
        {
            unitconversionScreen.SwitchToUnitConversion();
        }

        [When(@"I have selected types of units: (.*), (.*), (.*)")]
        public void WhenIHaveSelectedTypesOfUnits(string p0, string p1, string p2)
        {
            unitconversionScreen.SelectTypeofUnit1(p0);
            unitconversionScreen.SelectTypeofUnit2(p1);
            unitconversionScreen.SelectTypeofUnit3(p2);
        }

        [When(@"I have entered (.*) to be converted")]
        public void WhenIHaveEnteredToBeConverted(double p0)
        {
            unitconversionScreen.ClickFromField();
            unitconversionScreen.EnterNumber(p0);
        }

        [Then(@"the unit calculation result should be (.*)")]
        public void ThenTheUnitCalculationResultShouldBe(double expected)
        {
            var actual = unitconversionScreen.GetWhatsOnToField();
            Assert.AreEqual(expected, actual);
        }

    }
}
