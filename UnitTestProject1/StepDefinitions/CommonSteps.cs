﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetCrashCourseDay2;
using TechTalk.SpecFlow;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;

namespace UnitTestProject1.StepDefinitions
{
    [Binding]
    public sealed class CommonSteps
    {
        private CalcContext calcContext;
        private CalcSimpleScreen simpleScreen;

        public CommonSteps(CalcContext calcContext)
        {
            this.calcContext = calcContext;
            simpleScreen = new CalcSimpleScreen(calcContext.calcApp, calcContext.calc);
        }

        [AfterScenario]
        public void Cleanup()
        {
            var calc = calcContext.calc;
            var calcApp = calcContext.calcApp;

            simpleScreen.SwitchToSimple();
            simpleScreen.SwitchToBasicConversion();

            if (!calc.IsClosed)
            {
                calc.Close();
                calcApp.Close();
            }
        }
    }
}
