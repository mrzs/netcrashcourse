﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetCrashCourseDay2;
using TechTalk.SpecFlow;
using TestStack.White;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.WindowItems;

namespace UnitTestProject1.StepDefinitions
{
    [Binding]
    public sealed class CalculatorSteps
    {
        private CalcSimpleScreen simpleScreen;

        public CalculatorSteps(CalcContext calcContext)
        {
            simpleScreen = new CalcSimpleScreen(calcContext.calcApp, calcContext.calc);
        }

        [StepDefinition(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(double p1)
        {
            simpleScreen.EnterNumber(p1);
        }
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            simpleScreen.ClickAdd();
        }

        [When(@"I press equals")]
        public void WhenIPressEquals()
        {
            simpleScreen.ClickEquals();        
        }

        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(double expres)
        {
            double actualresult = simpleScreen.GetWhatsOnDisplay();
            Assert.AreEqual(expres, actualresult, "Expected result is not valid");
        }
    }
}
