﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;

namespace NetCrashCourseDay2
{
    public class CalcUnitConversionScreen : CalcSimpleScreen
    {
        private Application app;
        private Window wnd;

        public CalcUnitConversionScreen(Application app, Window wnd) : base(app,wnd)
        {
            this.app = app;
            this.wnd = wnd;
        }

        public void SelectTypeofUnitAll(string One, string Two, string Three, int Num)
        {
            SelectTypeofUnit1(One)
            .SelectTypeofUnit2(Two)
            .SelectTypeofUnit3(Three)
            .ClickFromField()
            .EnterNumber(Num);
        }

        public CalcUnitConversionScreen SelectTypeofUnit1(string type)
        {
            wnd.Get<ComboBox>(SearchCriteria.ByAutomationId("221")).Select(type);         
            return this;
        }

        public CalcUnitConversionScreen SelectTypeofUnit2(string type)
        {
            wnd.Get<ComboBox>(SearchCriteria.ByAutomationId("224")).Select(type);
            return this;
        }

        public CalcUnitConversionScreen SelectTypeofUnit3(string type)
        {
            wnd.Get<ComboBox>(SearchCriteria.ByAutomationId("225")).Select(type);
            return this;
        }

        public CalcUnitConversionScreen ClickFromField()
        {
            wnd.Get<TextBox>("From").Click();
            return this;
        }

        public double GetWhatsOnToField()
        {
            TextBox tofield = wnd.Get<TextBox>("To");
            return double.Parse(tofield.Text);
        }
    }
}
