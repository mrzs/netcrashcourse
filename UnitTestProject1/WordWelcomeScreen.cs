﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;

namespace UnitTestProject1
{
    class WordWelcomeScreen
    {
        private Application word;
        private Window welcomeScreen;

        public WordWelcomeScreen(Application word, Window mainWin)
        {
            this.word = word;
            this.welcomeScreen = mainWin;
        }

        public WordWelcomeScreen ClickOpenOtherDocuments()
        {
            return this;
        }
    }
}
