﻿Feature: Calculator
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@mytag @simple
Scenario Outline: Add two numbers
	Given I have entered <p1> into the calculator
	When I press add
	And I have entered <p2> into the calculator
	And I press equals
	Then the result should be <expres> on the screen

	Examples: 
	| p1 | p2 | expres |
	| 2  | 2  | 4      |
	| 3  | 3  | 6      |
	| 4  | 4  | 8      |
	| 5  | 5  | 10     |
	| 6  | 6  | 12     |
	| 7  | 7  | 14     |
	| 8  | 8  | 16     |
	| 9  | 9  | 18     |
	| 10 | 10 | 20     |

@unit
Scenario Outline: Converting miles to kilometers
	Given I have switched to unit conversion view
	When I have selected types of units: <type1>, <type2>, <type3>
	When I have entered <value> to be converted
	Then the unit calculation result should be <expres>

	Examples:
	| type1  | type2 | type3      | value | expres    |
	| Length | Mile  | Kilometers | 62    | 99,779328 |

