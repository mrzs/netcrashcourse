﻿using Innovative.SolarCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCrashCourseDay1
{   
    [TestClass]
    public class PavloTests
    {
        [TestInitialize]
        public void setup()
        {
            Console.WriteLine("Setup");
        }

        [TestCleanup]
        public void cleanup()
        {
            Console.WriteLine("Cleanup");
        }

        [DataTestMethod]
        [DataRow(49.83, 24.014167)]
        public void LvivSolarTest(double lat, double lon)
        {
            SolarTimes solarCalc = new SolarTimes();
            solarCalc.ForDate = DateTime.Parse("30.10.2017");
            solarCalc.Longitude = lon;
            solarCalc.Latitude = lat;

            var sunriseTime = solarCalc.Sunrise;

            Assert.AreEqual(sunriseTime.Hour, 7, "Calculated hour is incorrect!");
            Assert.AreEqual(sunriseTime.Minute, 9, "Calculated minute is incorrect!");
        }
    }
}
