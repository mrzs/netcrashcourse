﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.ListBoxItems;

namespace NetCrashCourseDay2
{
    [TestClass]
    public class WhiteTests
    {
        private Application calcApp;
        private Window calc;

        [TestMethod]
        public void MyFirstUITest()
        {
            double result =
                new CalcSimpleScreen(calcApp, calc)
                    .AddTwoNumbers(222.15, 200.999);
            Assert.AreEqual(423.149, result, "Expected result of 222.15 + 200.999 is not 423.149");
        }

        [TestMethod]
        public void MySecondUITest()
        {
            CalcScientificScreen calcScientific =
                new CalcScientificScreen(calcApp, calc)
                    .Navigate();

            calcScientific
                .EnterNumber(90);
            calcScientific
                .ClickSin();

            Assert.AreEqual("sind(90)",
                calcScientific.GetHistoryDisplay(),
                "Text is not valid");
            Assert.AreEqual(1,
                calcScientific.GetWhatsOnDisplay(),
                "Expected result of sin(90) is not 1");
        }

        [TestMethod]
        public void MyThirdUITest()
        {
            new CalcScientificScreen(calcApp, calc)
                .SwitchToUnitConversion()
                .SelectTypeofUnitAll("Length", "Mile", "Kilometers", 62);
            double result = new CalcUnitConversionScreen(calcApp, calc)
                .GetWhatsOnToField();
            Assert.AreEqual(99.779328, result, "Expected result doesn't match");
        }

        [TestInitialize]
            public void Preconditions()
            {
                calcApp = Application.Launch("calc.exe");
                calc = calcApp.GetWindow("Calculator");
                calc.Get<Menu>("View").Click();
                calc.Get<Menu>("Standard").Click();
                calc.Get<Menu>("View").Click();
                calc.Get<Menu>("Basic").Click();
            }

            [TestCleanup]
            public void Cleanup()
            {
                calc.Get<Menu>("View").Click();
                calc.Get<Menu>("Standard").Click();
                calc.Get<Menu>("View").Click();
                calc.Get<Menu>("Basic").Click();
                calcApp.Close();
            }
        }
    }
