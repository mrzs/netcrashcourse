﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;

namespace UnitTestProject1
{
    [TestClass]
    public class TestsWithPO
    {
        private Application word;
        private Window mainWin;

        [TestInitialize]
        public void Preconditions()
        {
            word = Application.Launch("WINWORD.EXE");
        }

        [TestCleanup]
        public void Cleanup()
        {
            word.Close();
        }
    }
}
