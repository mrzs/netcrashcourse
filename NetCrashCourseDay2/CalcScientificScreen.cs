﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.WindowItems;

namespace NetCrashCourseDay2
{
    public class CalcScientificScreen : CalcSimpleScreen
    {
        private Application app;
        private Window wnd;

        public CalcScientificScreen(Application app, Window wnd) : base(app,wnd)
        {
            this.app = app;
            this.wnd = wnd;
        }

        public double CalclateSin(int numOne)
        {
            return EnterNumber(numOne)
                .GetWhatsOnDisplay();
        }

        public CalcScientificScreen ClickSin()
        {
            wnd.Get<Button>("Sine").Click();
            return this;
        }

        
        public CalcScientificScreen Navigate()
        {
            SwitchToScientific();
            return this;
        }

    }
}
